/*
 * init.h
 *
 *  K�sz�lt: 2020.04.02
 *      K�sz�t�je: Vass D�vid Attila
 */


#ifndef INCLUDE_INIT_H_
#define INCLUDE_INIT_H_

// Lights
extern GLfloat light_position[];
extern GLfloat light_ambient[];
extern GLfloat light_diffuse[];
extern GLfloat light_specular[];

/**
 * Initialize the OpenGL context.
 */
void init();

#endif /* INCLUDE_INIT_H_ */
