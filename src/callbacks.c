/*
 * callbacks.c
 *
 *  K�sz�lt: 2020.04.02
 *      K�sz�t�je: Vass D�vid Attila
 */

#include <math.h>

#include "callbacks.h"
#include "init.h"
#include "draw.h"
#include "model.h"


int isHelpOn = 0;
double degree = 170;
int WINDOW_WIDTH;
int WINDOW_HEIGHT;

void display(void)
{
	double elapsed_time = calc_elapsed_time();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	update_camera_position(&camera, elapsed_time);
	draw_content(&world);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(60.0, (GLdouble)WINDOW_WIDTH / (GLdouble)WINDOW_HEIGHT, 0.1, 20000.0);
	glViewport(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);

	set_view_point(&camera);
	if(isHelpOn)
	{
		GLfloat torchForHelp[] = { 0.8, 0.8, 0.8, 0.8};
		glLightfv(GL_LIGHT1, GL_AMBIENT, torchForHelp);

		glLoadIdentity();
		gluOrtho2D(0, WINDOW_WIDTH, WINDOW_HEIGHT, 0);

		draw_help(world.helpTexture);

		glLightfv(GL_LIGHT1, GL_AMBIENT, light_ambient);
	}

    glutSwapBuffers();
}

void reshape(GLsizei width, GLsizei height)
{
	WINDOW_WIDTH = width;
	WINDOW_HEIGHT = height;
	
    set_clear_camera_pose(&camera);
}

void keyboard(unsigned char key, int x, int y)
{
	switch (key) {
	case 'w':
		action.move_forward = TRUE;
		break;
	case 's':
		action.move_backward = TRUE;
		break;
	case 'a':
		action.step_left = TRUE;
		break;
	case 'd':
		action.step_right = TRUE;
		break;
	case 'c':
		action.gravity=FALSE;
		action.move_down = TRUE;
		break;
	case 32:
		action.gravity=FALSE;
		action.move_up = TRUE;
		
		break;
	case '+':
		action.increase_light = TRUE;
		break;
	case '-':
		action.decrease_light = TRUE;
		break;
	case 'f':
		action.room_change = TRUE;
		break;
	case 'e':
		action.gravity=FALSE;
		action.chairpickup = TRUE;
		break;
	case 27:
		exit(0);
	}

	glutPostRedisplay();
}

void keyboardUp(unsigned char key, int x, int y)
{
	switch (key) {
	case 'w':
		action.move_forward = FALSE;
		break;
	case 's':
		action.move_backward = FALSE;
		break;
	case 'a':
		action.step_left = FALSE;
		break;
	case 'd':
		action.step_right = FALSE;
		break;
	case 'c':
		action.gravity=TRUE;
		action.move_down = FALSE;
		break;
	case 32:
		action.move_up = FALSE;
		break;
	case '+':
		action.increase_light = FALSE;
		break;
	case 'e':
		action.chairpickup = FALSE;
		action.gravity=TRUE;
		break;
	case '-':
		action.decrease_light = FALSE;
		break;
	}

	glutPostRedisplay();
}

void specialFunc(int key, int x, int y) {
	switch (key)
	{
		case GLUT_KEY_F1:
			if(isHelpOn == 1)
			{
				isHelpOn=0;
			}
			else isHelpOn = 1;
	}
}

void mouse(int button, int state, int x, int y)
{
	mouse_x = x;
	mouse_y = y;
}

void motion(int x, int y)
{
	double horizontal, vertical;

	horizontal = mouse_x - x;
	vertical = mouse_y - y;

	rotate_camera(&camera, horizontal, vertical);

	mouse_x = x;
	mouse_y = y;

	glutPostRedisplay();
}

void idle()
{
    glutPostRedisplay();
}

double calc_elapsed_time()
{
	static int last_frame_time = 0;
	int current_time;
	double elapsed_time;

	current_time = glutGet(GLUT_ELAPSED_TIME);
	elapsed_time = (double)(current_time - last_frame_time) / 1000.0;
	last_frame_time = current_time;


	return elapsed_time;
}

void update_camera_position(struct Camera* camera, double elapsed_time)
{
	float speed = 30;
	double distance;

	distance = elapsed_time * CAMERA_SPEED*speed;

	if (action.move_forward == TRUE) {
		move_camera_forward(camera, distance);
	}

	if (action.move_backward == TRUE) {
		move_camera_backward(camera, distance);
	}

	if (action.step_left == TRUE) {
		step_camera_left(camera, distance);
	}

	if (action.step_right == TRUE) {
		step_camera_right(camera, distance);
	}

	if (action.move_up == TRUE) {
		int size=200;
		double ugras=0.001;
		camera->prev_position = camera->position;
		if(camera->position.y<size-10)
		{
			
			action.antigravity=TRUE;
			
		}

		action.move_up=FALSE;
	}

	if (action.move_down == TRUE) {
		if(camera->position.y<=100)
		{
			if(camera->position.y>50)
			{
					camera->position.y-=3;
					
			}
			
			
		}
		//move_camera_down(camera, distance);
	}

	if (action.increase_light == TRUE) {
		if (light_ambient[0] < 1)
			light_ambient[0] = light_ambient[1] = light_ambient[2] += 0.15;
			glLightfv(GL_LIGHT1, GL_AMBIENT, light_ambient);
	}

	if (action.decrease_light == TRUE) {
		if (light_ambient[0] > -1.5)
			light_ambient[0] = light_ambient[1] = light_ambient[2] -= 0.15;
			glLightfv(GL_LIGHT1, GL_AMBIENT, light_ambient);
	}

	if (action.room_change == TRUE) {
		
		action.decrease_light = TRUE;
		world.huto.position.z+=1;
		if(world.huto.position.z>225 && world.huto.position.z<275)
		{
			action.decrease_light=FALSE;
			action.increase_light=TRUE;
			
		}
		else if(world.huto.position.z>275 && world.huto.position.z<350)
		{
			if(world.szek.position.x<170)
			{
				world.szek.position.x+=8;
			}
		if(world.szek.deg-90<=0)
			{
				world.szek.deg+=2;
			}
		if(world.szek.position.y-25<0)
			{
				world.szek.position.y+=0.75;
			}
		}
		else if(world.huto.position.z>350 && world.huto.position.z<400)
		{
		
			action.increase_light=FALSE;
			action.decrease_light=TRUE;
		}
		else if(world.huto.position.z>425 && world.huto.position.z<475)
		{
			world.roomfronts.position.x+=1;
			world.roombacks.position.x+=1;
			world.roomlefts.position.x+=1;
			action.decrease_light=FALSE;
			action.increase_light=TRUE;
			
		}
		else if(world.huto.position.z>500 && world.huto.position.z<550)
		{
			action.increase_light=FALSE;
			action.decrease_light=TRUE;
		}
		else if(world.huto.position.z>575 && world.huto.position.z<650)
		{
			action.candle=TRUE;
			action.decrease_light=FALSE;
			action.increase_light=TRUE;
		}
	}
	
	if(action.chairpickup==TRUE)
	{
		if(abs(camera->position.x)<170&& (camera->position.z>-195 && camera->position.z<150))
		{								
			if(!(abs((int)(camera->position.x - world.asztal.position.x)) < 70 && abs(((int)((camera->position.z)+20) - (world.asztal.position.z-30)))< 110 )&&!(abs((int)(camera->position.x- world.huto.position.x)) < 60 && abs((int)(((camera->position.z)+20) - world.huto.position.z))< 60 )&&!(abs((int)(camera->position.x- world.hutos.position.x)) < 60 && abs((int)(((camera->position.z)+20) - world.hutos.position.z))< 60 ))
			{
				if (abs((int)(camera->position.x - world.szek.position.x)) < 50 && abs((int)(camera->position.z - world.szek.position.z)) < 50 && camera->position.y < 150)
						{
							world.szek.position.z=camera->position.z+20;
							world.szek.position.x=camera->position.x;
							world.szek.position.y=camera->position.y-65;
							if(world.szek.deg>0)
							{
								world.szek.deg=0;
							}
						}
			}
			
		}
		/*koordinatazo
		double c=world.asztal.position.x;
		double d=world.asztal.position.z;
		double a=camera->position.y-(world.asztal.position.y);	
		double b=camera->position.z;
		printf("x:%f\n z:%f",a,b);*/
	}
	
	if(action.antigravity==TRUE)
	{
		if(camera->position.y<=150)
		{
					camera->position.y+=3;
			
		}
		else{
			if(camera->position.y>=150)
			{
				action.antigravity=FALSE;
				action.gravity=TRUE;
			}
		}
		
	}
	
	if(action.gravity==TRUE)
	{
		if(world.szek.deg==0)
		{
			if(world.szek.position.y!=0)
			{
				if(world.szek.position.y>0)
				{
						world.szek.position.y-=2;
				}
				if(world.szek.position.y<0)
					world.szek.position.y=0;
			}
		}
		if(camera->position.y!=100)
		{
			if(camera->position.y>100)
			{
					camera->position.y-=3;
			}
			if(camera->position.y<100)
			{
				camera->position.y+=3;
			}
		}
	}
	if(action.candle==TRUE)
	{
		if(world.lampa.position.y<=150)
		{
			world.lampa.position.y+=1;
			world.tuz.position.y+=1;
		}
		else{
		degree+=1;
		double angle = degree_to_radian(degree);
		world.lampa.position.x=cos(angle)*150;
		world.tuz.position.x=cos(angle)*150;
		world.lampa.position.z=sin(angle)*150;
		world.tuz.position.z=sin(angle)*150;
		}
	}
	
	
}
